<?php



//Первый вариант записи массива и цикла перебора
$arr = array(array('Иванов','Иван','Иванович',"22.10.1930",), //>40
    array('Петров','Петр','Петрович',"01.01.1999",), //<40
    array('Сидоров','Олег','Олегович',"26.10.2001",), // <40
    array('Алексеев','Алексей','Алексеевич',"12.12.1460",), //>40
    array('Лукьянчиков','Виктор','Сергеевич',"01.01.1981",), //=40
    array('Струков','Юрий','Михайлович',"01.01.1970",),); // >40
$now_date = date("d.m.Y");



/*
for($i=0;$i<count($arr);$i++)
{
    $my_date = strtotime($arr[$i][3]);
    $now_date_un = strtotime($now_date);
    $res = ($now_date_un - $my_date)/(60*60*24*365);
    $years = floor($res);
    if($years>40){
        echo "<divfloat>";
        for ($j=0;$j<count($arr[0]);$j++){


            echo $arr[$i][$j];
            echo "  ";}
        echo "</div>";
    }
    elseif ($years<40){
        echo "<div>";
        for ($j=0;$j<count($arr[0]);$j++){
            echo $arr[$i][$j];
            echo "  ";}
        echo "</div>";
    }
}
*/
//Не получившееся
/*function date_diff($date1, $date2): float|int
{
    $res = strtotime($date2) - strtotime($date1)/(60*60*24*365);
    $years = floor($res);
    return abs($years);
}*/
//Второй вариант записи массива и цикла

$user = array(
    array(
        "Фамилия" => "Студёнов",
        "Имя" => "Николай",
        "Отчество" => "Петрович",
        "Дата рождения" => "22.10.1920",
    ),
    array(
        "Фамилия" => "Антонов",
        "Имя" => "Василий",
        "Отчество" => "Геннадьевич",
        "Дата рождения" => "11.03.2000",
    ),
    array(
        "Фамилия" => "Гордеев",
        "Имя" => "Сергей",
        "Отчество" => "Сергеевич",
        "Дата рождения" => "09.09.2002",
    ),
    array(
        "Фамилия" => "Петров",
        "Имя" => "Петр",
        "Отчество" => "Петрович",
        "Дата рождения" => "05.10.1900",
    ),
    array(
        "Фамилия" => "Васильева",
        "Имя" => "Татьяна",
        "Отчество" => "Николаевна",
        "Дата рождения" => "16.11.1958",
    ),
    array(
        "Фамилия" => "Стрекалов",
        "Имя" => "Дмитрий",
        "Отчество" => "Александрович",
        "Дата рождения" => "22.10.1930",
    ),
    array(
        "Фамилия" => "Комарицкий",
        "Имя" => "Сергей",
        "Отчество" => "Александрович",
        "Дата рождения" => "02.02.2004",
    ),
);
//echo $user[0]["Имя"]; Проверяю можно ли так выводить массив, оказывается можно :D
?>
<DOCTYPE! html>
<html lang="ru">
<head>
    <title>Тестовое задание</title>
    <style>
        .div-left {
            border: 1px solid black;
            float: left;
            width: 40%;
            height: 100px;
            text-align: center;
        }
        .div-right{
            border: 1px solid black;
            float: left;
            width: 40%;
            height: 100px;
            text-align: center;
        }
    </style>
</head>
<body>
<div class="main_div">
    <div class='div-left'> <b>>40</b>
        <?php for($i=0;$i<count($arr);$i++)
        {
            $my_date = strtotime($arr[$i][3]); //наверное это можно было объединить все в функцию для высчитывания времени, но я додумалась слишком поздно
            $now_date_un = strtotime($now_date);
            $res = ($now_date_un - $my_date)/(60*60*24*365);
            $years = floor($res);
            if($years>40){
                echo "<br />";
                for ($j=0;$j<count($arr[0]);$j++){


                    echo $arr[$i][$j];
                    echo "  ";}

            }
        }
        ?>
    </div>
    <div class="div-right"><b><40</b>
        <?php for($i=0;$i<count($arr);$i++)
        {
            $my_date = strtotime($arr[$i][3]);
            $now_date_un = strtotime($now_date);
            $res = ($now_date_un - $my_date)/(60*60*24*365);
            $years = floor($res);
            if($years<40){
                echo "<br />";
                for ($j=0;$j<count($arr[0]);$j++){


                    echo $arr[$i][$j];
                    echo "  ";}

            }
        }
        ?>
    </div>
    <div class='div-left'> <b>>40</b>
        <?php
        for ($m = 0; $m < count($user); $m++){

            foreach ($user[$m] as $key => $value){

                if($key == "Дата рождения"){
                    /*echo $value;*/
                    $my_date = strtotime($value);
                    $now_date_un = strtotime($now_date);
                    $res = ($now_date_un - $my_date)/(60*60*24*365);
                    $years = floor($res);
                    if($years > 40){
                        echo "<div>";
                        echo $user[$m]["Фамилия"];
                        echo "  ";
                        echo $user[$m]["Имя"];
                        echo "  ";
                        echo $user[$m]["Отчество"];
                        echo "  ";
                        echo $user[$m]["Дата рождения"];
                        echo "  ";
                        echo "</div>";
                    }
                }
            }
        }

        ?>
    </div>
    <div class='div-right'> <b><40</b><?php
        for ($m = 0; $m < count($user); $m++){

            foreach ($user[$m] as $key => $value){

                if($key == "Дата рождения"){
                    /*echo $value;*/
                    $my_date = strtotime($value);
                    $now_date_un = strtotime($now_date);
                    $res = ($now_date_un - $my_date)/(60*60*24*365);
                    $years = floor($res);
                    if($years < 40){
                        echo "<div>";
                        echo $user[$m]["Фамилия"];
                        echo "  ";
                        echo $user[$m]["Имя"];
                        echo "  ";
                        echo $user[$m]["Отчество"];
                        echo "  ";
                        echo $user[$m]["Дата рождения"];
                        echo "  ";
                        echo "</div>";
                    }
                }
            }
        } //Мне очень не нравится, что у меня получается, но поделать пока ничего не могу, лучше сдать такое
        //и потом исправлять всё, нежели не сдать совсем после и так прогорелых дедлайнов
        ?>
    </div>
    <div class="div-left"><?php
       echo array_search("26.10.2001", $arr);   //В 1:20 мне все перестало отвечать :(
        //Идеи были сделать через array_search, но сейчас в 1:12 умные мысли покинули мою голову,
        //надеюсь доделаю нормально на выходных
        //Идея array_search в
        ?>



       </div>
    <div></div>
</div>
</body>
</html>